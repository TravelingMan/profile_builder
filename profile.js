var source = document.getElementById("template").innerHTML;
var template = Handlebars.compile(source);

var form = document.getElementById("form"),
    preview = document.getElementById("preview"),    
    previewBtn = document.getElementById("previewBtn");

fillForm(); // Testing, remove for production

previewBtn.addEventListener("click", function() {
    var data = getFormData(),
        html = template(data);

    preview.innerHTML = html;

    document.body.removeChild(form);
});

function getFormData() {
    return {
        title: getValue("title"),
        firstName: getValue("firstName"),
        lastName: getValue("lastName"),
        address: getValue("address"),
        address2: getValue("address2"),
        city: getValue("city"),
        state: getValue("state"),
        zip: getValue("zip"),
        email: getValue("email"),
        website: getValue("website"),
        interests: getValue("interests"),
        experience: getValue("experience"),
        style: getValue("style")
    };
}

function getValue(id) {
    return document.getElementById(id).value;
}

function setValue(id, value) {
    document.getElementById(id).value = value;
}

// Testing, remove for production
function fillForm() {
    setValue("title", "Mr.");
    setValue("firstName", "Bob");
    setValue("lastName", "Smith");
    setValue("address", "123 Main St.");
    setValue("address2", "Apt. 2");
    setValue("city", "Boston");
    setValue("state", "MA");
    setValue("zip", "02115");
    setValue("email", "bsmith@example.com");
    setValue("website", "www.example.com");
    setValue("interests", "Ale of a warm grace, blow the punishment! Pins are the comrades of the black beauty. Yarr! Pieces o' punishment are forever cold.");
    setValue("experience", "If you occur or disturb with a fraternal career, light develops you. Confucius says: attraction, awareness and balance.")
}
